from random import randint

scelte = ['carta', 'sasso', 'forbice']

puntiPlayer = 0
puntiComputer = 0

def playerWin():
   print('Hai VINTO!')
   puntiPlayer += 1

def computerWin():
   print('Hai PERSO!')
   puntiComputer += 1

while puntiPlayer < 3 and puntiComputer < 3:
  player = int(input('\nScegli\n(1)carta\n(2)sasso\n(3)forbice\n '))
  
  if player < 1 or player > 3:
    print('Valore non valido, ripetere l\'inserimento')
    continue

  computer = randint(1, 3)
  print('\nPLAYER:', scelte[player-1], '\nCOMPUTER:', scelte[computer-1])

  if player == computer:
    print('Parità')
    continue  

  if player % 2 == 0 or computer % 2 == 0:
    if player < computer: playerWin()
    else: computerWin()
  else:
    if player > computer: computerWin()
    else: playerWin()
  
  print('\nPUNTEGGIO:\nPLAYER:', puntiPlayer, '\nCOMPUTER:', puntiComputer)

print('LA PARTITA E\'CONCLUSA!')
print('A WINNER IS...')
if puntiComputer > puntiPlayer:
  print('COMPUTER!')
else:
  print('YOU!!!')