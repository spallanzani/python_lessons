nome = input('Come ti chiami? ')
print('Ciao',nome,'! Questo è il tuo primo programma!')
anni = int(input('Quanti anni hai? '))

if anni > 18:
  print(nome, 'sei maggiorenne da', anni-18,'anni!')
elif anni == 18:
  print(nome, 'sei appena diventato maggiorenne!')  
else:
  print(nome, 'ti mancano', 18-anni, 'anni per diventare maggiorenne!')

if anni % 2 == 0:
  print('hai un numero di anni PARI!')
else:
  print('hai un numero di anni DISPARI!')  