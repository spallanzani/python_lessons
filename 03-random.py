from random import randint
persone = [
  'Tizio',
  'Caio',
  'Sempronio',
  'Maria',
  'Laura',
  'Teresa'
]
interrogati = []
numeroInterrogazioni = int(input('quanti sono gli interrogati? '))
if numeroInterrogazioni == 0:
  numeroInterrogazioni = len(persone)  

for i in range(numeroInterrogazioni):
  rnd = randint(0, len(persone)-1)
  interrogato = persone.pop(rnd)
  print('ho estratto il numero', rnd+1,'che corrisponde a', interrogato)
  interrogati.append(interrogato)

print('Sono stati interrogati: ')
for interrogato in interrogati:
  print(interrogato)

if len(persone) > 0:
  print('Sono rimasti da interrogare: ')
  for persona in persone:
    print(persona)
else:
  print('Non ci sono altre persone da interrogare')