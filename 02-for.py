#Si possono iterare i range:
for i in range(11):
  print('3 x',i,'=',3*i)

print()

#Si possono iterare le stringhe:
nome = 'Giovanni'
for lettera in nome:
  print('Datemi una', lettera,'!')
print(nome,'!!!')

print()

#Si possono iterare le liste:
carrello = ['mele', 'pere', 'banane']
for prodotto in carrello:
  print('hai comperato', prodotto)

print()

#Possiamo interrompere in qualunque momento un ciclo for con l'istruzione break
carrello = ['banane', 'mele', 'caramelle', 'carote', 'uva']
for prodotto in carrello:
  if prodotto == 'caramelle':
    print('Ehi! non puoi comperare delle caramelle! Vai a riporle!')
    break
  print('hai comperato', prodotto)

print('Vado a riporre le caramelle e mi prendo la cioccolata')
print()
carrello.remove('caramelle')
carrello.append('cioccolata')

for prodotto in carrello:
  print(prodotto)